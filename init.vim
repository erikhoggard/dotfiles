" HEY! run ':help provider-python' and install pynvim
call plug#begin('~/AppData/Local/nvim/plugged')
" below are some vim plugin for demonstration purpose
" Plug 'joshdick/onedark.vim'
" Plug 'iCyMind/NeoSolarized'
"
 Plug 'vim-airline/vim-airline'
 Plug 'vim-airline/vim-airline-themes'
 Plug 'maksimr/vim-jsbeautify'
 Plug 'valloric/youcompleteme'
 Plug 'vim-airline/vim-airline'
 Plug 'scrooloose/nerdtree'
 Plug 'jelera/vim-javascript-syntax'
 Plug 'raimondi/delimitmate'
 Plug 'easymotion/vim-easymotion'

 call plug#end()

"===========AUTO ON STARTUP==========
colorscheme desert
set number
set hlsearch
set ignorecase
let g:airline_powerline_fonts = 1


"===========JSBEAUTIFY=============
map <c-f> :call JsBeautify()<cr>
" or
autocmd FileType javascript noremap <buffer>  <c-f> :call JsBeautify()<cr>
" for json
autocmd FileType json noremap <buffer> <c-f> :call JsonBeautify()<cr>
" for jsx
autocmd FileType jsx noremap <buffer> <c-f> :call JsxBeautify()<cr>
" for html
autocmd FileType html noremap <buffer> <c-f> :call HtmlBeautify()<cr>
" for css or scss
autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>


"========NERDTREE=========
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeDirArrowExpandable = '⮞'
let g:NERDTreeDirArrowCollapsible = '⮟'
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"autocmd vimenter * NERDTree













































